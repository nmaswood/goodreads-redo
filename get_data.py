import csv
import requests as r
from bs4 import BeautifulSoup
from pymongo import MongoClient
import json
from time import sleep
from Seeds import Seeds
import os

run = Seeds()
extract_data_from_csv = run.extract_data_from_csv


# 1. CON reviewers of CON books
# 2. LIB reviewers of LIB books
# 3. CON reviewers of BOTH books
# 4. LIB reviewers of BOTH books


class Data():

    def __init__(self):
        self.client = MongoClient('localhost', 27017)
        self.db = self.client['GOODREADS']

    def get_reviews(self):

        pairs = [

            ('C_REVIEWS_PROCESSED','con','CON_REVIEWS_CON_SEPT_2016_INDIV_REVIEW/{}-{}.txt','CON_REVIEWS_CON_SEPT_2016_INDIV.csv', 'CON_REVIEWS_CON_SEPT_2016_AGG_REVIEW/{}.txt','CON_REVIEWS_CON_SEPT_2016_AGG.csv'),
            ('L_REVIEWS_PROCESSED','lib', 'LIB_REVIEWS_LIB_SEPT_2016_INDIV_REVIEW/{}-{}.txt','LIB_REVIEWS_LIB_SEPT_2016_INDIV.csv', 'LIB_REVIEWS_LIB_SEPT_2016_AGG_REVIEW/{}.txt','LIB_REVIEWS_LIB_SEPT_2016_AGG.csv'),

            ('C_REVIEWS_PROCESSED','both', 'CON_REVIEWS_BOTH_SEPT_2016_INDIV_REVIEW/{}-{}.txt','CON_REVIEWS_BOTH_SEPT_2016_INDIV.csv', 'CON_REVIEWS_BOTH_SEPT_2016_AGG_REVIEW/{}.txt','CON_REVIEWS_BOTH_SEPT_2016_AGG.csv'),
            ('L_REVIEWS_PROCESSED','both', 'LIB_REVIEWS_BOTH_SEPT_2016_INDIV_REVIEW/{}-{}.txt','LIB_REVIEWS_BOTH_SEPT_2016_INDIV.csv', 'LIB_REVIEWS_BOTH_SEPT_2016_AGG_REVIEW/{}.txt','LIB_REVIEWS_BOTH_SEPT_2016_AGG.csv'),
        ]


        for mongo_src, csv_src, indiv_folder, indiv_csv, agg_folder, agg_csv in pairs:

            for folder in [indiv_folder, agg_folder]:
                f = folder.split("/",1)[0]
                if not os.path.exists(f):
                    os.mkdir(f)

            with open(agg_csv,'w') as agg_csv_fp:

                writer = csv.writer(agg_csv_fp)
                writer.writerow(['idx','book_name'])

                for idx,book_name in enumerate(extract_data_from_csv(csv_src)):

                    book_data = self.db[mongo_src].find({"book_name": book_name}, no_cursor_timeout = True)
                    reviews = [book.get('review') for book in book_data if book.get('review') is not None]
                    writer.writerow([idx,book_name])

                    for idx_prime,review in enumerate(reviews):
                        with open(indiv_folder.format(idx,idx_prime), 'w') as indiv_folder_fp:
                            print (review)
                            try:
                                indiv_folder_fp.write(review)
                            except:
                                pass

                    with open(agg_folder.format(idx), 'w') as agg_folder_fp:
                        try:
                            content = ' '.join(reviews)

                            if "ERROR" in content and len(content) == 1:
                                pass
                            else:
                                agg_folder_fp.write(content)
                        except:
                            pass
run = Data()
run.get_reviews()
