import csv
import requests as r
from bs4 import BeautifulSoup
from pymongo import MongoClient
import json
from time import sleep

class Seeds():

    def __init__(self):
        self.client = MongoClient('localhost', 27017)
        self.db = self.client['GOODREADS']

    def extract_data_from_csv(self,csv_type):

        fp = {
            "lib": "three/Distinctive_LIB_Top100_updated.csv",
            "con": "three/Distinctive_CON_Top100_updated.csv",
            "both": "three/Distinctive_BOTH_Top100_updated_prime_prime.csv",
        }[csv_type]


        file_type = 'rb' if csv_type == 'both' else 'r'

        with open(fp, file_type) as infile:

            if csv_type == 'both':
                books = []
                for line in infile:
                    try:
                        x = str(line, 'utf-8').strip('\r\n').replace(';',',')
                        books.append(x)
                    except:
                        pass

                books.append("Miss Peregrine’s Home for Peculiar Children (Miss Peregrine’s Peculiar Children; #1)".replace(
                    ';',','))

                return books


            else:
                reader = csv.reader(infile)
                next(reader)

                return [x[1].replace(';', ',') for x in reader]